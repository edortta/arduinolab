#include <EtherCard.h>

#define STATIC 1  // set to 1 to disable DHCP (adjust myip/gwip values below)
/*
 * http://www.vwlowen.co.uk/arduino/router-monitor/router-monitor.htm
 * https://www.aelius.com/njh/ethercard/ether_node_8ino-example.html
 * https://components101.com/microcontrollers/arduino-uno
 */


// mac address
static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };
// ethernet interface ip address
static byte myip[] = { 192,168,11,17 };
// gateway ip address
static byte gwip[] = { 192,168,11,2 };

// LED to control output
#define greenLedPin 5
#define redLedPin 6
#define yellowLedPin 7

#define relayPin 4

int states[5];
int blink_status = LOW;

// STATUS
#define can_ping 1
#define relay_active 2
#define relay_inactive 3
int status = can_ping;

#define CS_PORT 10
int initialized = 0;
int errorCount = 0;
int redLedStatus = LOW;

#define pingTimeout   1000
#define errorTimeout  5000
#define maxErrorCount 10
#define errorTurnOverFactor 2
#define errorTurnOverCount (maxErrorCount * errorTurnOverFactor)
#define relayWaitTime  10000
#define relayResetTime 30000

byte Ethernet::buffer[800];
unsigned long timer;
unsigned long last_pong;
unsigned long relay_start;


char const page[] PROGMEM = "HTTP/1.0 503 Service Unavailable\r\n"
"Content-Type: text/html\r\n"
"Retry-After: 600\r\n"
"\r\n"
"<html>"
  "<head><title>"
    "Service Temporarily Unavailable"
  "</title></head>"
  "<body>"
    "<h3>This service is currently unavailable</h3>"
    "<p><em>"
      "The main server is currently off-line.<br />"
      "Please try again later."
    "</em></p>"
  "</body>"
"</html>"
;

int ledStatus(int ledName, int newStatus) {
  int ledNdx=-1;
  int n;

  if (ledName==greenLedPin)
    ledNdx=1;
  else if (ledName==yellowLedPin)
    ledNdx=2;
  else if (ledName==redLedPin)
    ledNdx=3;
  else if (ledName==relayPin)
    ledNdx=4;
  else
    ledNdx=0;

  if ((newStatus==HIGH) || (newStatus==LOW)) {
    if (newStatus != states[ledNdx]) {
      digitalWrite(ledName, newStatus);
      states[ledNdx] = newStatus;

      Serial.print(errorCount);
      Serial.print(F(" > "));

      for (n=0; n<4; n++) {
        Serial.print(states[n]);
        Serial.print(F(" "));
      }
      Serial.println();  

    }  
  }
  return states[ledNdx];
}

void setup () {
  Serial.begin(9600);
  if (initialized==0) {
    initialized=1;
    
    states[0]=-1;

    pinMode(greenLedPin, OUTPUT);
    pinMode(yellowLedPin, OUTPUT);
    pinMode(redLedPin, OUTPUT);
    pinMode(relayPin, OUTPUT);
    
    turn_all_leds(LOW);
    ledStatus(relayPin, LOW);
    
    turn_all_leds(HIGH);
    
    Serial.println(F("Trying to configure the IP..."));
  
    Serial.print(F("MAC: "));
    for (byte i = 0; i < 6; ++i) {
      Serial.print(mymac[i], HEX);
      if (i < 5)
        Serial.print(F(":"));
    }
    Serial.println(F(" - "));

    ledStatus(redLedPin, LOW);
    
    if (ether.begin(sizeof Ethernet::buffer, mymac, CS_PORT) == 0) 
      Serial.println(F("Failed to access Ethernet controller"));
    else
      Serial.println(F("Ethetnet controller ready"));
  
    ledStatus(greenLedPin, LOW);
  
    #if STATIC
      Serial.println( F("Setting static IP."));
      if (!ether.staticSetup(myip, gwip)){
        Serial.println( F("could not get a static IP"));
        while(true) {
          blinkLeds();     // blink forever to indicate a problem        
          delay(500);
        }
      }
    
    #else
    
      Serial.println(F("Setting up DHCP"));
      if (!ether.dhcpSetup()){
        Serial.println(F("DHCP failed"));
        blinkLed();     // blink forever to indicate a problem
      }
    #endif
    ledStatus(yellowLedPin, LOW);

    ether.parseIp(ether.hisip, "192.168.11.2");
    ether.printIp(F("IP..: "), ether.myip);
    ether.printIp(F("Mask: "), ether.netmask);
    ether.printIp(F("GW..: "), ether.gwip);
    ether.printIp(F("DNS.: "), ether.dnsip);
    ether.printIp(F("TGT.: "), ether.hisip);

    ether.registerPingCallback(gotPinged);
    last_pong = timer = -9999999;
  }
}

void turn_all_leds(int led_stat) {
  ledStatus(greenLedPin, blink_status);
  ledStatus(yellowLedPin, blink_status);
  ledStatus(redLedPin, blink_status);  
}

void blinkLeds(){
  if (blink_status == HIGH) {
    blink_status = LOW;
  } else {
    blink_status = HIGH;
  }
  turn_all_leds(blink_status);
}

static void gotPinged (byte* ptr) {
  ledStatus(yellowLedPin, HIGH);
  ether.printIp(F(">>> ping from: "), ptr);
  delay(100);
  ledStatus(yellowLedPin, LOW);
  delay(50);
}

static void showWastedTime(const char* lbl, unsigned int ms) {
    Serial.print(F(" "));
    Serial.print(lbl);
    Serial.print(F(" "));
    Serial.print(ms);
    Serial.println(F(" ms"));   
}

unsigned int now() {
  return micros() / 1000;
}

void resetStatuses() {  
  #if DEBUG
    Serial.println("Reseting status");
  #endif
  ledStatus(greenLedPin,HIGH);
  ledStatus(redLedPin,LOW);
  ledStatus(yellowLedPin, LOW);
  ledStatus(relayPin, LOW);
  errorCount=0;  
  timer = last_pong = now();
}

void loop () {

    unsigned int _now_ = now();

    unsigned int pongWastedTime = ( _now_ - last_pong );
    unsigned int wastedTime =     ( _now_ - timer );

    if (wastedTime % 1000 == 0) {
      Serial.print(F("status: "));
      Serial.print(status);
      Serial.print(F(" wastedTime: "));
      Serial.print(wastedTime);
      Serial.print(F(" pongWastedTime: "));
      Serial.print(pongWastedTime);
      Serial.print(F(" errorCount: "));
      Serial.println(errorCount);
    }

    if (status == can_ping) {
      if (wastedTime >= pingTimeout) {
        ledStatus(greenLedPin,LOW);
        
        #if DEBUG
          ether.printIp(F("Pinging: "), ether.hisip);
        #endif
        
        wastedTime = timer = _now_;
        ether.clientIcmpRequest(ether.hisip);
      }    
    }
  
   
    word len = ether.packetReceive();
    word pos = ether.packetLoop(len);
  
    if (len>0) {
      if (ether.packetLoopIcmpCheckReply(ether.hisip)) {

        #if DEBUG
          showWastedTime("withoutPong", pongWastedTime);
        #endif
        resetStatuses();

        #if DEBUG
          showWastedTime("pong response", wastedTime);
        #endif
      }
    }
  
    if ( pongWastedTime > errorTimeout) {
  
      if (pongWastedTime % 500 == 0) {
        if (status==can_ping)
          ledStatus(greenLedPin,LOW);

        showWastedTime("pong timeout",pongWastedTime);
        ledStatus(yellowLedPin, HIGH);      
        errorCount++;
        if (errorCount>maxErrorCount) {

          if (status == can_ping) {
            if (redLedStatus == HIGH)
              redLedStatus = ledStatus(redLedPin,LOW);
            else
              redLedStatus = ledStatus(redLedPin,HIGH);

          }

          if (errorCount > errorTurnOverCount) {

            if (status==can_ping) {
              #if DEBUG
                Serial.println(F("---- [ RELAY ON ] ----"));
              #endif 

              status=relay_active;

              ledStatus(relayPin, HIGH);  
              relay_start = _now_;
              
            }

            if (status==relay_active) {
              if (_now_ - relay_start > relayResetTime) {
                turn_all_leds(LOW);
                redLedStatus = ledStatus(redLedPin,HIGH);
                #if DEBUG
                  Serial.println(F("---- [ RELAY OFF ] ----"));
                #endif
                
                ledStatus(relayPin, LOW);
                status = relay_inactive;
                relay_start = _now_;
              } else {
                blinkLeds();
              }
            }

            if (status==relay_inactive) {
              if (_now_ - relay_start > relayWaitTime) {
                #if DEBUG
                  Serial.println(F("---- [ ping again ] ----"));
                #endif

                status = can_ping;
                resetStatuses();                
              }
            }

          }
        }
      }
    }    
  
  
    // IF LED10=ON turn it ON
    if (strstr((char *)Ethernet::buffer + pos, "GET /?LED10=ON") != 0) {
        Serial.println(F("Received ON command"));
        ledStatus(greenLedPin, HIGH);
      }
  
      // IF LED10=OFF turn it OFF  
      if(strstr((char *)Ethernet::buffer + pos, "GET /?LED10=OFF") != 0) {
        Serial.println(F("Received OFF command"));
        ledStatus(greenLedPin, LOW);
      }
  
      // show some data to the user
      memcpy_P(ether.tcpOffset(), page, sizeof page);
      ether.httpServerReply(sizeof page - 1);    



}
