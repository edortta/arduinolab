#include <SPI.h>  
#include <DS1307.h>
#include <Ethernet.h>
#include <ICMPPing.h>


#include <SD.h>

// set up variables using the SD utility library functions:
Sd2Card card;
SdVolume volume;
SdFile root;

// change this to match your SD shield or module;
// Arduino Ethernet shield: pin 4
// Adafruit SD shields and modules: pin 10
// Sparkfun SD shield: pin 8
const int chipSelect = 4;   

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // max address for ethernet shield
//byte ip[] = {192,168,2,177}; // ip address for ethernet shield
byte pingAddr[] = {172,16,5,1}; // ip address to ping

DS1307 rtc(20, 21);

char mob_num[]="xxxxx"; //Phone number

SOCKET pingSocket = 0;

char buffer [256];

void setup()
{
   Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  Serial.print("\nInitializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
  pinMode(10, OUTPUT);     // change this to 53 on a mega


  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("* is a card is inserted?");
    Serial.println("* Is your wiring correct?");
    Serial.println("* did you change the chipSelect pin to match your shield or module?");
    while(1);
  } else {
   Serial.println("Wiring is correct and a card is present."); 
  }

	
	for(int i=0;i<8;i++)
	{
		pinMode(22+i,OUTPUT);
	}
	for(int j=0;j<8;j++)
	{
		digitalWrite(22+j,HIGH);
		delay(500);
		digitalWrite(22+j,LOW);
		delay(500);
	}
	pinMode(9,OUTPUT);
	digitalWrite(9,HIGH);
	delay(500);
	digitalWrite(9,LOW);
	delay(500);	
	pinMode(48,OUTPUT);
	digitalWrite(48,HIGH);
	delay(500);
	digitalWrite(48,LOW);
	delay(500);			
	
	rtc.halt(false);

// The following lines can be commented out to use the values already stored in the DS1307
    rtc.setDOW(SUNDAY);        // Set Day-of-Week to SUNDAY
    rtc.setTime(12, 0, 0);     // Set the time to 12:00:00 (24hr format)
    rtc.setDate(3, 10, 2010);   // Set the date to October 3th, 2010
	
	delay (1000);	
	for(int x=0;x<8;x++)
	{
  // Send Day-of-Week
  Serial.print(rtc.getDOWStr());
  Serial.print(" ");
  
  // Send date
  Serial.print(rtc.getDateStr());
  Serial.print(" -- ");

  // Send time
  Serial.println(rtc.getTimeStr());
  
  // Wait one second before repeating :)
  delay (1000);	
	
	}
	
	Ethernet.begin(mac);
	delay(5000);
  for(int i=0;i<10;i++)
  {
  ICMPPing ping(pingSocket);
  ping(4, pingAddr, buffer);
  Serial.println(buffer);
  delay(300);
  }	
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  
  Serial.println("Making calls.......");
  
  
	Serial1.begin(9600);
	for(int i = 0;i<4;i++)
	{
          Serial1.print("ATD");
          Serial1.print(mob_num);
          Serial1.println(";");
		  delay(200);
		  while(Serial1.available())
		  {
			Serial.write(Serial1.read());
		  }
          delay(2000); 
	}
	

}


void loop()
{


}