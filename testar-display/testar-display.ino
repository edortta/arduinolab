/*
  LiquidCrystal Library - Hello World

 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.

 This sketch prints "Hello World!" to the LCD
 and shows the time.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int current_relay = 0;
char relay_names[8][16];


boolean last_white_state = LOW;
boolean current_white_state = LOW;
const int WHITE_BUTTON = 7;

boolean last_red_state = LOW;
boolean current_red_state = LOW;
const int RED_BUTTON = 6;

boolean debounce(int button, boolean last) {
  boolean current = digitalRead(button);
  if (last != current) {
    delay(5);
    current = digitalRead(button);
  }
  return current;
}

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("InoSis");

  lcd.setCursor(8,0);
  lcd.print("*-------");

  strcpy(relay_names[0], "Speedy-1");
  strcpy(relay_names[1], "Speedy-2");
  strcpy(relay_names[2], "Link");
  strcpy(relay_names[3], "MigoNet");
  strcpy(relay_names[4], "Blnceadr");
  strcpy(relay_names[5], "Server-1");
  strcpy(relay_names[6], "Server-2");
  strcpy(relay_names[7], "Router");
}

void loop() {
  char current_relay_name[16];
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(millis() / 1000);

  
  // white button (select relay)
  current_white_state = debounce(WHITE_BUTTON, last_white_state);
  if ((current_white_state == LOW) && (current_white_state != last_white_state)) {
    lcd.setCursor(8+current_relay,0);
    lcd.print("-");
    current_relay = (current_relay + 1 ) % 8;
    
    lcd.setCursor(8+current_relay,0);
    lcd.print("*");

    sprintf(current_relay_name, "%8s",relay_names[current_relay]);
    lcd.setCursor(8,1);
    lcd.print(current_relay_name);
  }
  last_white_state = current_white_state;


  // red button (reset the relay)
  current_red_state = debounce(RED_BUTTON, last_red_state);
  if ((current_red_state != last_red_state) && (current_red_state == LOW)) {
    lcd.setCursor(0,0);
    lcd.print("RESET ");
    delay(3000);
    lcd.setCursor(0,0);
    lcd.print("InoSis");
  }
  last_red_state = current_red_state;
}
